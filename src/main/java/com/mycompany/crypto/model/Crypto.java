package com.mycompany.crypto.model;


public class Crypto {
    private final String name;
    private final String symbol;
    private final float price;

    public Crypto(String name, String symbol, float price) {
        this.name = name;
        this.symbol = symbol;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getSymbol(){
        return symbol;
    }

    public float getPrice() {
        return price;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Crypto{");
        sb.append("name='").append(name).append('\'');
        sb.append(", symbol=").append(symbol).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
}
