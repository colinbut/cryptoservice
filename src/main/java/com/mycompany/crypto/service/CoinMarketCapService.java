package com.mycompany.crypto.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;

@Service
public class CoinMarketCapService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CoinMarketCapService.class);

    private static final String MARKET_DATA_QUOTE_URL = "cryptocurrency/quotes/latest";

    @Value("${api.host.baseurl}")
    private String marketDataApiHost;

    @Value("${currency}")
    private String currency;

    @Autowired
    private RestTemplate restTemplate;

    public JsonNode getMarketData(String symbols) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("X-CMC_PRO_API_KEY", System.getenv("CMC_API_KEY"));

        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(marketDataApiHost + MARKET_DATA_QUOTE_URL)
                .queryParam("symbol", symbols)
                .queryParam("convert", currency)
                .build();

        HttpEntity<?> request = new HttpEntity<>(null, headers);

        LOGGER.info("Making requests to: {}", uriComponents.toUriString());
        ResponseEntity<String> response = restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, request, String.class);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode root = objectMapper.readTree(response.getBody());
        JsonNode btc = root.path("data").path("BTC");
        return root;
    }
}
