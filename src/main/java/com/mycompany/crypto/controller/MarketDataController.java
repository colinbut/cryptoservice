package com.mycompany.crypto.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.mycompany.crypto.model.Crypto;
import com.mycompany.crypto.service.CoinMarketCapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MarketDataController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CoinMarketCapService.class);

    @Autowired
    private CoinMarketCapService coinMarketCapService;

    @GetMapping("/market")
    public List<Crypto> getMarketData() {
        String[] symbols = {"BTC", "ETH", "ADA", "FIL", "TRX", "SOL", "AVAX", "LINK", "THETA"};
        List<Crypto> cryptos = new ArrayList<>();
        try {
            JsonNode jsonResponse = coinMarketCapService.getMarketData(String.join(",", symbols));

            for(String symbol: symbols){
                JsonNode cryptoData = jsonResponse.path("data").path(symbol);
                String name = cryptoData.path("name").toString();
                float price = cryptoData.path("quote").path("USD").path("price").floatValue();
                Crypto crypto = new Crypto(name, symbol, price);
                LOGGER.debug("Fetched crypto data for: {}", crypto);
                cryptos.add(crypto);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cryptos;
    }
}
